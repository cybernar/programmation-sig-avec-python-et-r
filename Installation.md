# Installation des packages avec conda

Pour tester les notebooks, il vous faudra une distribution Python 3 avec les packages jupyter, rasterio, geopandas. 

Le plus simple est d'installer **Miniconda** ou **Anaconda**, et de créer un environnement avec les packages requis.

- [Miniconda](https://docs.conda.io/en/latest/miniconda.html) est un gestionnaire d'environnements qui vous permettra d'installer Python et les packages dans un environnement cloisonné. C'est une version minimaliste de conda qui fonctionne en lignes de commande.
- [Anaconda](https://anaconda.cloud/installers) est une version interface graphique de conda, orientée "analyse de données". Elle inclut des environnements de développement tels que Spyder, Jupyter.

Que ce soit l'un ou l'autre, nous recommandons d'utiliser l'**invite de commandes** pour créer l'environnement requis. 

**Remarques** : l'utilisation du dépôt `conda-forge` est fortement recommandée afin d'obtenir la dernière version des packages, en particulier de **rasterio**. Enfin, l'utilisation de la commande `mamba` permet de gagner beaucoup de temps dans l'installation des packages, car elle accélère la résolution des dépendances.

## 1. Création d'un environnement geo_env avec Python 3.8 et mamba

    conda create -n geo_env -c conda-forge python=3.8 mamba

## 2. Activation du dépôt geo_env

    conda activate geo_env

## 3. Installation des packages numpy, pandas, gdal, rasterio, geopandas et autres

    mamba install numpy scipy matplotlib seaborn pandas openpyxl numba
    mamba install jupyterlab
    mamba install shapely fiona gdal rasterio geopandas rasterstats sentinelsat rtree

## 4. Installation du package pyrasta

    pip install pyrasta

Note : pour lancer jupyter, se mettre dans le répertoire de la formation (avec `cd`), et activer l'environnement `geo_env` 

    conda activate geo_env
    cd dossier_travail/programmation-sig-avec-python-et-r
    jupyter lab
