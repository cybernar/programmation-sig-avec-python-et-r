# Programmation SIG avec Python et R

Projet de cours en ligne sur la programmation appliquée aux données spatiales avec Python et R.

UMR Espace-Dev, 2022

## Sommaire (provisoire)

### `notebooks/python_01_rasterio.ipynb`

**Opérations de base sur les rasters**

- chargement des modules
- ouvrir un fichier raster, exemple 1 : un fichier '.jp2'
- lire les propriétés d'un raster
- ouvrir un fichier raster, exemple 2 : un fichier '.asc'
- lire les propriétés sous forme de dictionnaire avec meta et profile
- la matrice de transformation
- afficher les données (approche simple)
- charger les données d'1 bande raster dans 1 tableau numpy
- techniques de slicing appliquées aux données raster
- extraire une fenêtre avec le module windows
- fermer un raster

**Traitements sur les rasters**

- comment trouver les valeurs min et max d'1 bande, ainsi que d'autres informations sur les valeurs ?
- comment explorer la distribution des valeurs dans un raster ?
- comment utiliser numpy comme calculatrice raster ?
- comment enregistrer le résultat d'un calcul dans un nouveau fichier raster ?
- comment créer un jeu de données raster en mémoire ?
- comment fusionner des dalles rasters ?
- comment reprojeter un raster ?
- comment calculer les statistiques focales (fenêtre glissante sur les pixels) ?

**Interactions vecteur - raster**

- comment rastériser des polygones vectoriels ?
- comment masquer une partie d'un raster ?
- comment faire des statistiques zonales sur les polygones ?

### `notebooks/python_01_rasterio_s2.ipynb`

**Application sur une image Sentinel 2**

- lecture des bandes d'une image Sentinel-2 (Openhub Copernicus)
- comment extraire une fenêtre sans charger l'ensemble des données ?
- comment générer 1 raster multibandes à partir de 4 rasters monobandes ?
- comment calculer les quantiles avec les valeurs de chaque bande ?
- comment ajuster les valeurs pour visualiser l'image en RGB ?
- comment calculer un indice à partir des bandes d'1 raster ? Exemple : le NDVI
- comment lire les données et calculer un indice bloc par bloc ?

### `notebooks/python_02_pyrasta.ipynb`

**Traitements sur les rasters avec pyrasta**

* Ouvrir et afficher les propriétés d'un fichier raster
* Charger les données dans un tableau numpy
* Extraire les données d'1 raster sur un point 
* Afficher les données d'1 raster
* Extraire une fenêtre avec clip
* Comment fusionner des rasters ?
* Comment reprojeter un raster ?
* Calculer les statistiques d'1 raster
* Visualiser la distribution des pixels
* Statistiques focales
* Calcul de pente sur 1 MNT
* Comment rastériser un shapefile ?
* Comment utiliser un shapefile comme masque ?
* Comment calculer des statistiques zonales ?
* Calculatrice raster 
* Sauvegarder un tableau numpy dans un fichier GeoTiff

### `notebooks/python_03_geopandas.ipynb`

**Structure des données**

- chargement des modules
- comment lire une fichier Shapefile ?
- quelles sont les propriétés d'1 objet `GeoDataFrame` et comment est stockée la partie géométrique des données ? (index et colonnes, types, CRS)
- utilisation de loc et iloc

**Manipulations courantes sur les données vectorielles**

- comment filtrer des données sur la base d'un attribut ?
- comment visualiser des données vectorielles avec geopandas ?
- comment reprojeter des données ?
- comment calculer la longueur ou la surface d'une entité ?
- comment supprimer une colonne ?
- comment supprimer une ligne ?
- comment faire une requête spatiale ?
- et une jointure spatiale ? (exemple sélectionner des points qui intersectent un ensemble de polygones)
- comment créer un jeu de données géoréférencé à partir d'un fichier CSV qui contient 2 colonnes latitude / longitude ?
- comment intersecter 2 jeux de données (exemple intersecter des lignes et des polygones pour calculer la longueur totale de lignes dans chaque polygone)
