# Ressources web

## Python

- Python Data Science Handbook (Jake VanderPlas) : <https://jakevdp.github.io/PythonDataScienceHandbook/index.html>

## R

- R for Data Science (Hadley Wickham) : <https://r4ds.had.co.nz/>
- Geocomputation with R (Robin Lovelace) : <https://geocompr.robinlovelace.net/>

