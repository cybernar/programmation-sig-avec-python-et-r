# Données utilisées dans les notebooks

Les données utilisées dans les notebooks sont publiées en licence ouverte et disponibles en libre accès sur le web.

## Liste des données

**Données IGN :**

Les données IGN utilisées dans les notebooks sont publiées en Licence ouverte Etalab 2.0 (LO) sur le site <https://geoservices.ign.fr>.

- BP TOPO Département Shapefile - D066 Pyrénées-Orientales - Décembre 2021 (extrait)
- BD ALTI 25 m - Pyrénées-Orientales (extrait)
- OCS GE - Département 66 - Pyrénées-Orientales

**Données Opendata SNCF :** 

Les données sont publiées en licence Open Database License (ODbL) sur la plateforme Opendata de la SNCF 

- Référentiel SNCF des gares de voyageurs (export format CSV) : <https://ressources.data.sncf.com/explore/dataset/referentiel-gares-voyageurs/table/>
- Fréquentation en gares (export format CSV) : <https://ressources.data.sncf.com/explore/dataset/frequentation-gares/table/>
- Fichier de formes des lignes du Réseau Ferré National (export format Shapefile) : <https://ressources.data.sncf.com/explore/dataset/formes-des-lignes-du-rfn/map/>

**Données Sentinel-2 Copernicus**

- Image Sentinel-2 du 2021-08-20 des Pyrénées-Orientales, niveau de traitement 2A, téléchargées sur le site https://scihub.copernicus.eu (création d'un compte nécessaire). Pour les besoins des TP, l'image peut être téléchargée ici : lien ; le fichier .zip est à décompressé dans un répertoire `Copernicus`.



## Nomenclature et chemin d'accès des fichiers

    notebooks/data
    ├── IGN
    │   ├── BDALTIV2_MNT_25M_ASC_LAMB93_IGN69_D066
    │   │   ├── BDALTIV2_25M_FXX_0625_6175_MNT_LAMB93_IGN69.asc
    │   │   ├── BDALTIV2_25M_FXX_0625_6200_MNT_LAMB93_IGN69.asc
    │   │   ├── BDALTIV2_25M_FXX_0650_6175_MNT_LAMB93_IGN69.asc
    │   │   ├── BDALTIV2_25M_FXX_0650_6200_MNT_LAMB93_IGN69.asc
    │   │   ├── BDALTIV2_25M_FXX_0675_6175_MNT_LAMB93_IGN69.asc
    │   │   ├── BDALTIV2_25M_FXX_0675_6200_MNT_LAMB93_IGN69.asc
    │   │   ├── BDALTIV2_25M_FXX_0700_6175_MNT_LAMB93_IGN69.asc
    │   │   └── BDALTIV2_25M_FXX_0700_6200_MNT_LAMB93_IGN69.asc
    │   ├── BDT_3-0_SHP_LAMB93_D066-ED2021-03-15
    │   │   └── ADMINISTRATIF
    │   │       ├── ARRONDISSEMENT.cpg
    │   │       ├── ARRONDISSEMENT.dbf
    │   │       ├── ARRONDISSEMENT.prj
    │   │       ├── ARRONDISSEMENT.shp
    │   │       ├── ARRONDISSEMENT.shx
    │   │       ├── COMMUNE.cpg
    │   │       ├── COMMUNE.dbf
    │   │       ├── COMMUNE.prj
    │   │       ├── COMMUNE.shp
    │   │       ├── COMMUNE.shx
    │   │       ├── DEPARTEMENT.cpg
    │   │       ├── DEPARTEMENT.dbf
    │   │       ├── DEPARTEMENT.prj
    │   │       ├── DEPARTEMENT.shp
    │   │       ├── DEPARTEMENT.shx
    │   │       ├── EPCI.cpg
    │   │       ├── EPCI.dbf
    │   │       ├── EPCI.prj
    │   │       ├── EPCI.shp
    │   │       ├── EPCI.shx
    │   │       ├── REGION.cpg
    │   │       ├── REGION.dbf
    │   │       ├── REGION.prj
    │   │       ├── REGION.shp
    │   │       └── REGION.shx
    │   └── OCSGE_1-1_SHP_LAMB93_D66_2015
    │       ├── OCCUPATION_SOL.cpg
    │       ├── OCCUPATION_SOL.dbf
    │       ├── OCCUPATION_SOL.prj
    │       ├── OCCUPATION_SOL.shp
    │       ├── OCCUPATION_SOL.shx
    │       ├── ZONE_CONSTRUITE.cpg
    │       ├── ZONE_CONSTRUITE.dbf
    │       ├── ZONE_CONSTRUITE.prj
    │       ├── ZONE_CONSTRUITE.shp
    │       └── ZONE_CONSTRUITE.shx
    └── SNCF
        ├── formes-des-lignes-du-rfn
        │   ├── formes-des-lignes-du-rfn.dbf
        │   ├── formes-des-lignes-du-rfn.prj
        │   ├── formes-des-lignes-du-rfn.shp
        │   └── formes-des-lignes-du-rfn.shx
        ├── frequentation-gares.csv
        └── referentiel-gares-voyageurs.csv
